describe('Registration tests', () => {
    it('Register', () => {

        // Visit the Gallery App
        cy.visit('https://gallery-app.vivifyideas.com/')

        // Click on Register
        cy.contains('Register').click()

        // Verify that url has changed
        cy.url().should('include', '/register')

        // Enter value Iva in field First Name
        cy.get('#first-name')
            .clear()
            .type('Iva')
            .should('have.value', 'Iva')

        cy.get('#last-name')
            .clear()
            .type('Ivic')
            .should('have.value', 'Ivic')
            
        cy.get('#email')
            .clear()
            .type('iva@mail.com')
            .should('have.value', 'iva@mail.com')

        cy.get('#password')
            .clear()
            .type('lozinka11')
            .should('have.value', 'lozinka11')

        cy.get('#password-confirmation')
            .clear()
            .type('lozinka11')
            .should('have.value', 'lozinka11')
            
        // Check checkbox
        cy.get('.form-check-input')
            .check()
            .should('be.checked')

        // Click Submit Button
        cy.get('.btn')
            .click()

        // Check if login was successful by displaying Logout
        cy.get('.ml-auto > :nth-child(3) > .nav-link')
            .should('have.text', '\n            Logout\n          ')
    })

    it('Register existing email', () => {

        // Visit the Gallery App
        cy.visit('https://gallery-app.vivifyideas.com/')

        // Click on Register
        cy.contains('Register').click()

        // Verify that url has changed
        cy.url().should('include', '/register')

        // Enter value Iva in field First Name
        cy.get('#first-name')
            .clear()
            .type('Iva')
            .should('have.value', 'Iva')

        // Enter value Ivic in field Last Name
        cy.get('#last-name')
            .clear()
            .type('Ivic')
            .should('have.value', 'Ivic')
        
        // Enter used email
        cy.get('#email')
            .clear()
            .type('iva@mail.com')
            .should('have.value', 'iva@mail.com')

        // Enter password with 9 characters
        cy.get('#password')
            .clear()
            .type('lozinka11')
            .should('have.value', 'lozinka11')

        cy.get('#password-confirmation')
            .clear()
            .type('lozinka11')
            .should('have.value', 'lozinka11')
            
        // Check checkbox
        cy.get('.form-check-input')
            .check()
            .should('be.checked')

        // Click Submit Button
        cy.contains('Submit')
            .click()

        // Check the alert message
        cy.get('.alert')
            .should('have.text', 'The email has already been taken.')

    })

    it('Register password 7 characters', () => {

        // Visit the Gallery App
        cy.visit('https://gallery-app.vivifyideas.com/')

        // Click on Register
        cy.contains('Register').click()

        // Verify that url has changed
        cy.url().should('include', '/register')

        cy.get('#first-name')
            .clear()
            .type('Iva')
            .should('have.value', 'Iva')

        cy.get('#last-name')
            .clear()
            .type('Ivic')
            .should('have.value', 'Ivic')
            
        cy.get('#email')
            .clear()
            .type('iva@gmail.com')
            .should('have.value', 'iva@gmail.com')

        // Enter password with 7 characters
        cy.get('#password')
            .clear()
            .type('lozinka')
            .should('have.value', 'lozinka')

        cy.get('#password-confirmation')
            .clear()
            .type('lozinka')
            .should('have.value', 'lozinka')
            
        // Check checkbox
        cy.get('.form-check-input')
            .check()
            .should('be.checked')

        // Click Submit Button
        cy.contains('Submit')
            .click()

        // Check the alert message
        cy.get('.alert')
            .should('have.text', 'The password must be at least 8 characters.')

    })

    it('Password confirmation does not match', () => {

        // Visit the Gallery App
        cy.visit('https://gallery-app.vivifyideas.com/')

        // Click on Register
        cy.contains('Register').click()

        // Verify that url has changed
        cy.url().should('include', '/register')

        cy.get('#first-name')
            .clear()
            .type('Iva')
            .should('have.value', 'Iva')

        cy.get('#last-name')
            .clear()
            .type('Ivic')
            .should('have.value', 'Ivic')
            
        cy.get('#email')
            .clear()
            .type('iva@gmail.com')
            .should('have.value', 'iva@gmail.com')

        cy.get('#password')
            .clear()
            .type('lozinka1')
            .should('have.value', 'lozinka1')

        // Enter different password
        cy.get('#password-confirmation')
            .clear()
            .type('lozinka2')
            .should('have.value', 'lozinka2')
            
        // Check checkbox
        cy.get('.form-check-input')
            .check()
            .should('be.checked')

        // Click Submit Button
        cy.contains('Submit')
            .click()

        // Check the alert message
        cy.get('.alert')
            .should('have.text', 'The password confirmation does not match.')

    })

    it('Register terms and conditions not checked', () => {

        // Visit the Gallery App
        cy.visit('https://gallery-app.vivifyideas.com/')

        // Click on Register
        cy.contains('Register').click()

        // Verify that url has changed
        cy.url().should('include', '/register')

        cy.get('#first-name')
            .clear()
            .type('Iva')
            .should('have.value', 'Iva')

        cy.get('#last-name')
            .clear()
            .type('Ivic')
            .should('have.value', 'Ivic')
            
        cy.get('#email')
            .clear()
            .type('iva@gmail.com')
            .should('have.value', 'iva@gmail.com')

        cy.get('#password')
            .clear()
            .type('lozinka1')
            .should('have.value', 'lozinka1')

        cy.get('#password-confirmation')
            .clear()
            .type('lozinka1')
            .should('have.value', 'lozinka1')
            
        // Confirm checkbox is not checked
        cy.get('.form-check-input')
            .should('not.be.checked')

        // Click Submit Button
        cy.contains('Submit')
            .click()

        // Check the alert message
        cy.get('.alert')
            .should('have.text', 'The terms and conditions must be accepted.')
    })

    it('Password 8 characters', () => {

        // Visit the Gallery App
        cy.visit('https://gallery-app.vivifyideas.com/')

        // Click on Register
        cy.contains('Register').click()

        // Verify that url has changed
        cy.url().should('include', '/register')

        // Enter value in field First Name
        cy.get('#first-name')
            .clear()
            .type('Mika')
            .should('have.value', 'Mika')

        cy.get('#last-name')
            .clear()
            .type('Mikic')
            .should('have.value', 'Mikic')
            
        cy.get('#email')
            .clear()
            .type('mika@mail.com')
            .should('have.value', 'mika@mail.com')

        cy.get('#password')
            .clear()
            .type('lozinka8')
            .should('have.value', 'lozinka8')

        cy.get('#password-confirmation')
            .clear()
            .type('lozinka8')
            .should('have.value', 'lozinka8')
            
        // Check checkbox
        cy.get('.form-check-input')
            .check()
            .should('be.checked')

        // Click Submit Button
        cy.get('.btn')
            .click()

        // Check if login was successful by displaying Logout
        cy.get('.ml-auto > :nth-child(3) > .nav-link')
            .should('have.text', '\n            Logout\n          ')

    })


    
})