describe('Create Gallery test', () => {

    beforeEach(()=> {
        cy.visit('https://gallery-app.vivifyideas.com/')
        cy.signIn('iva@mail.com', 'lozinka11')
    })

    it('Create Gallery', () => {

        // Click on Create Gallery
        cy.get('.mr-auto > :nth-child(3) > .nav-link')
            .click()
        
        // Verify that url has changed
        cy.url().should('include', '/create')

        // Enter title
        cy.get('#title')
            .clear()
            .type('My Gallery')
            .should('have.value', 'My Gallery')
        
        // Enter Descriptions
        cy.get('#description')
            .clear()
            .type('My Gallery of Sunsets')
            .should('have.value', 'My Gallery of Sunsets')

        // Enter image url
        cy.get('.input-group > .form-control')
            .clear()
            .type('http://kucaboja.rs/291-large_default/fototapet-pacific-sunset-218.jpg')

        // Click on Add image
        cy.get('form > :nth-child(3) > :nth-child(3)')
            .click()
        
        // Enter second image url
        cy.get(':nth-child(3) > .input-group > .form-control')
            .clear()
            .type('https://hotelmanager.rs/wp-content/uploads/2019/04/sunset-675847_960_720.jpg')

        // Click Submit Button
        cy.get('form > :nth-child(4)')
            .click()

        cy.wait(300)

        // Check if they are now in My Galleries
        // Click on My Galleries
        cy.get(':nth-child(2) > .nav-link')
            .click()

        // Verify that url has changed
        cy.url().should('include', '/my-galleries')        

        // Check how many images are on the page
        cy.get('img')
            .should('have.length', 1)
        
        // Click on My Gallery picture title
        cy.get('h2 > .box-title')
            .click()
        
        // Verify that url has changed
        cy.url().should('include', '/galleries/517')

        cy.get('.carousel-control-next-icon')
            .click()
        
        // Check if both images are added
        cy.get('#carousel___BV_indicator_2_')
            .should('exist')
    })

    it('Edit Gallery Delete one image', () => {

        cy.wait(300)
        
        // Click on My Galleries
        cy.get(':nth-child(2) > .nav-link')
            .click()
        
        // Verify that url has changed
        cy.url().should('include', '/my-galleries')
        
        // Click on My Gallery title
        cy.get('h2 > .box-title')
            .click()

        // Verify that url has changed
        cy.url().should('include', '/galleries/517')
        
        // Clik on Edit Gallery
        cy.get('a.btn')
            .click()

        // Verify that url has changed
        cy.url().should('include', '/edit-gallery/517')

        // Click on recycle bin of second image
        cy.get(':nth-child(3) > .input-group > .input-group-append > :nth-child(1) > .fas')
            .click()
        
        // Click Submit Button
        cy.get('form > :nth-child(4)')
            .click()

        cy.wait(300)

        // Click on My Galleries
        cy.get(':nth-child(2) > .nav-link')
            .click()
        
        // Click on My Gallery title
        cy.get('h2 > .box-title')
            .click()
        
        // Check if now only one picture has left
        cy.get('#carousel___BV_indicator_2_')
            .should('not.exist')
    })

    it('Edit Gallery Add new image', () => {

        cy.wait(300)
        
        // Click on My Galleries
        cy.get(':nth-child(2) > .nav-link')
            .click()
        
        // Verify that url has changed
        cy.url().should('include', '/my-galleries')
        
        // Click on My Gallery title
        cy.get('h2 > .box-title')
            .click()

        // Verify that url has changed
        cy.url().should('include', '/galleries/517')
        
        // Clik on Edit Gallery
        cy.get('a.btn')
            .click()

        // Verify that url has changed
        cy.url().should('include', '/edit-gallery/517')

        // Click on Add image
        cy.get('form > :nth-child(3) > :nth-child(3)')
            .click()
        
        // Enter new image url
        cy.get(':nth-child(3) > .input-group > .form-control')
            .clear()
            .type('https://hotelmanager.rs/wp-content/uploads/2019/04/sunset-675847_960_720.jpg')

        // Click Submit Button
        cy.get('form > :nth-child(4)')
            .click()

        cy.wait(300)

        // Click on My Galleries
        cy.get(':nth-child(2) > .nav-link')
            .click()

        // Verify that url has changed
        cy.url().should('include', '/my-galleries')        

        // Check how many images are on the page
        cy.get('img')
            .should('have.length', 1)
        
        // Click on My Gallery picture title
        cy.get('h2 > .box-title')
            .click()
        
        // Verify that url has changed
        cy.url().should('include', '/galleries/517')

        cy.get('.carousel-control-next-icon')
            .click()
        
        // Check if new image is added
        cy.get('#carousel___BV_indicator_2_')
            .should('exist')


    })

    it('Edit Gallery change title', () => {

        cy.wait(300)
        
        // Click on My Galleries
        cy.get(':nth-child(2) > .nav-link')
            .click()
        
        // Verify that url has changed
        cy.url().should('include', '/my-galleries')
        
        // Click on My Gallery title
        cy.get('h2 > .box-title')
            .click()

        // Verify that url has changed
        cy.url().should('include', '/galleries/517')
        
        // Clik on Edit Gallery
        cy.get('a.btn')
            .click()

        // Verify that url has changed
        cy.url().should('include', '/edit-gallery/517')

        // Change title
        cy.get('#title')
            .clear()
            .type('My new Gallery')

        // Click Submit Button
        cy.get('form > :nth-child(4)')
            .click()

        cy.wait(300)

        // Click on My Galleries
        cy.get(':nth-child(2) > .nav-link')
            .click()

        // Verify that url has changed
        cy.url().should('include', '/my-galleries')        

        // Check title
        cy.get('h2 > .box-title')
            .should('have.text', '\n          My new Gallery\n        ')

    })

    it('Delete Gallery', () => {

        cy.wait(300)

        // Click on My Galleries
        cy.get(':nth-child(2) > .nav-link')
            .click()
        
        // Verify that url has changed
        cy.url().should('include', '/my-galleries')
        
        // Click on My Gallery title
        cy.get('h2 > .box-title')
            .click()

        // Click on Delete Gallery
        cy.get(':nth-child(5) > button.btn')
            .click()
        
        // Check that gallery does not exist anymore
        cy.get('#__BVID__45 > .d-block')
            .should('not.exist')

    })

})