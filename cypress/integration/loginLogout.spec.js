describe('Login and Logout tests', () => {
    it('Login', () => {

        // Visit the Gallery App
        cy.visit('https://gallery-app.vivifyideas.com/')

        // Click on Login
        cy.contains('Login').click()

        // Verify that url has changed
        cy.url().should('include', '/login')

        // Enter value in Email field
        cy.get('#email')
            .clear()
            .type('iva@mail.com')
            .should('have.value', 'iva@mail.com')

        // Enter value in Password field
        cy.get('#password')
            .clear()
            .type('lozinka11')
            .should('have.value', 'lozinka11')

        // Click on Submit Button
        cy.get('.btn')
            .click()
        
        // Check if login was successful by displaying Logout
        cy.get('.ml-auto > :nth-child(3) > .nav-link')
            .should('have.text', '\n            Logout\n          ')
    })

    it('Logout', () => {

        // Visit the Gallery App
        cy.visit('https://gallery-app.vivifyideas.com/')

        // Click on Login
        cy.contains('Login').click()

        // Verify that url has changed
        cy.url().should('include', '/login')

        // Enter value in Email field
        cy.get('#email')
            .clear()
            .type('iva@mail.com')
            .should('have.value', 'iva@mail.com')

        // Enter value in Password field
        cy.get('#password')
            .clear()
            .type('lozinka11')
            .should('have.value', 'lozinka11')

        // Click on Submit Button
        cy.get('.btn')
            .click()

        cy.wait(300)

        // Click on Logout
        cy.get('.ml-auto > :nth-child(3) > .nav-link')
            .click()

        // Verify that url has changed
        cy.url().should('include', '/login')
        
    }) 

    it('Login incorrect email', () => {

        // Visit the Gallery App
        cy.visit('https://gallery-app.vivifyideas.com/')

        // Click on Login
        cy.contains('Login').click()

        // Verify that url has changed
        cy.url().should('include', '/login')

        // Enter incorrect value in Email field
        cy.get('#email')
            .clear()
            .type('iv@mail.com')
            .should('have.value', 'iv@mail.com')

        // Enter value in Password field
        cy.get('#password')
            .clear()
            .type('lozinka11')
            .should('have.value', 'lozinka11')

        // Click on Submit Button
        cy.get('.btn')
            .click()

        // Check the alert message
        cy.get('.alert')
            .should('have.text', 'Bad Credentials')
    })

    it('Login incorrect password', () => {

        // Visit the Gallery App
        cy.visit('https://gallery-app.vivifyideas.com/')

        // Click on Login
        cy.contains('Login').click()

        // Verify that url has changed
        cy.url().should('include', '/login')

        // Enter value in Email field
        cy.get('#email')
            .clear()
            .type('iva@mail.com')
            .should('have.value', 'iva@mail.com')

        // Enter incorrect value in Password field
        cy.get('#password')
            .clear()
            .type('lozinka1')
            .should('have.value', 'lozinka1')

        // Click on Submit Button
        cy.get('.btn')
            .click()

        // Check the alert message
        cy.get('.alert')
            .should('have.text', 'Bad Credentials')
    })
})