describe('All Galleries test', () => {

    beforeEach(()=> {
        cy.visit('https://gallery-app.vivifyideas.com/')
        cy.signIn('iva@mail.com', 'lozinka11')
    })

    it('Add comment below specific picture', () => {

        // Click on All Galleries
        cy.contains('All Galleries')
            .click()

        // Click on Moj grad at first picture 
        cy.get(':nth-child(1) > h2 > .box-title')
            .click()
        
        // Verify that url has changed
        cy.url().should('include', '/galleries/498')

        // Add comment in textarea
        cy.get('textarea')
            .type('Nice photo')
            .should('have.value', 'Nice photo')
        
        // Click on Submit Button
        cy.contains('Submit')
            .click()
            
        // Check if comment is there
        cy.get('.list-group-item > :nth-child(1)')
            .should('have.text', 'Nice photo')
    })

    it('Add empty comment', () => {

        // Click on All Galleries
        cy.contains('All Galleries')
            .click()

        // Click on Moj grad at first picture 
        cy.get(':nth-child(1) > h2 > .box-title')
            .click()
     
        // Verify that url has changed
        cy.url().should('include', '/galleries/498')

        // Add comment in textarea
        cy.get('textarea')
            .type(' ')
            .should('have.value', ' ')
     
        // Click on Submit Button
        cy.contains('Submit')
            .click()

        // Check the alert message
        cy.get('.alert')
            .should('have.text', 'The body field is required.')

    })

    it('Delete comment', () => {

        // Click on All Galleries
        cy.contains('All Galleries')
            .click()

        // Click on Moj grad at first picture 
        cy.get(':nth-child(1) > h2 > .box-title')
            .click()
        
        // Verify that url has changed
        cy.url().should('include', '/galleries/498')

        // Click on recycle bin
        cy.get(':nth-child(1) > div > button')
            .click()
        
        // Check that element does't exist anymore
        cy.get('.list-group-item > :nth-child(1)')
            .should('not.exist')
    })

    it('Search Filter', () => {

        // Click on All Galleries
        cy.contains('All Galleries')
            .click()

        // Type in Search
        cy.get('.input-group > .form-control:not(:last-child), .input-group > .custom-select:not(:last-child)')
            .type('novi sad 21')
            .should('have.value', 'novi sad 21')
         
        // Click on Filter
        cy.get('.input-group-append > .btn')
            .click()    

        // Check how many images are on the page
        cy.get('img')
            .should('have.length', 4)
    })

    it.skip('Load More Button', () => {

        // Click on All Galleries
        cy.contains('All Galleries')
            .click()

        // Click on Load More Button
        cy.get(':nth-child(3) > :nth-child(2) > .btn')
            .click()
        
        // Check if now there is ten more images
        cy.get('img')
            .should('have.length', 20)

        // Click again on Load More
        cy.get(':nth-child(3) > :nth-child(2) > .btn')
            .click()

        cy.get('img')
            .should('have.length', 30)

        // doing this till the end
    })

    it.skip('Click on author', () => {

        //click on specific author
        cy.get(':nth-child(1) > p > .box-title')
            .should('have.text', '\n          Maja Ilic\n        ')
            .click()

        cy.get('.title-style')
            .should('have.text', 'Galleries of Maja  Ilic')
    })




})